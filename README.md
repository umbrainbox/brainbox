# Brainbox Project

## Description
The Brainbox project aims to provide a common computing platform for fMRI researchers at UM that is resizable and relocatable, to allow for scalable computing of typical fMRI data processing. The software collection is primarily provided through the [NeuroDebian](http://neuro.debian.net/) project with special additions for the UM computing environment. These additions include UM Kerberos user login, licensed software, security options, network filesystem mounts, and more. 

## Documentation
Under construction.

## Installation 

This iteration of the BrainBox is based on Xubuntu 16.04, Xenial, which is a long-term
support (LTS) release.  The following options from the Xubuntu installation were used
to install the machines.

* Check both the "download updates" and the "install third-party" boxes at the
  "Preparing to Install" dialog
* Check "Erase disk and install Xubuntu" from the "Installation Type" dialog;
  We don't at this time encrypt the disk, nor use LVM
* At the "Erase Disk and Install Xubuntu", select the appropriate disk for you
  installation.  For the Yottabyte Research Cloud, this should be Virtual disk 1
* Select "Install Now"
* At the Time Zone dialog, type Detroit in the box, and an item for Detroit will
  complete.  Select that.
* For Keyboard layout, choose English (US) in both columns
* At the Who Are You box, complete this using an _admin account_ that will have
  administrative privileges, _do not use your regular login name_; require a
  password to log in

The installation should begin after that selection.  Once it is complete, it will
prompt you to remove the installation media and reboot.

Once the installation has succeeded, log into your new machine with the admin account,
open a Terminal window, and execute the following commands (you will be prompted for
the admin account's password after the first 'sudo' command).

    $ sudo apt-get update
    $ sudo apt-get upgrade
    $ sudo apt-get install -y git

The following will fetch the latest files and run all the setup scripts.

    git clone https://bitbucket.org/umbrainbox/brainbox.git
    cd brainbox/scripts
    sudo ./setup-all

You can also run a singular setup script with the necessary environment variable set. Please see the INSTALL file for more information.

## Notes
We are aware of better ways to do configuration management than with these kinds of setup scripts. These exist to allow fast prototyping and testing before we migrate to our preferred configuration management solution (Ansible).

## Contact
email: <fMRI.VDI.project@umich.edu>

## Contributors
Mark Champe (<mrchampe@umich.edu>), Bennet Fauber (<bennet@umich.edu>), Daniel Kessler (<kesslerd@umich.edu>)

## License
brainbox is Copyright (C) 2016 Regents of The University of Michigan.


Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of The University of Michigan shall not be used in advertising or otherwise to promote the sale, use or other dealings in this Software without prior written authorization from the Regents of The University of Michigan.