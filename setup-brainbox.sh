#!/bin/bash
# To step through the installer, set DEBUG="yes"
# DEBUG="no"
DEBUG="yes"
# Runs all the setup scripts
# Assumes a new, working Ubuntu 16.04 LTS install

export BB_TOP="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export SCRIPTS=$BB_TOP/scripts

echo BB_TOP is $BB_TOP
echo Scripts are in $BB_TOP/$SCRIPTS

bb_debbug () {
    if [ "$DEBUG" == "yes" ]; then
        echo "Continue?"
        read x
    fi
}

echo
echo "==== Brainbox configuration begins..."
echo

exit 0
# Basic Ubuntu setup
source $SCRIPTS/setup-utils.sh

# install firewall before ssh is enabled
source $SCRIPTS/setup-ufw.sh

# ssh configuration
source $SCRIPTS/setup-ssh.sh

# NTP
source $SCRIPTS/setup-ntp.sh

# Kerberos
source $SCRIPTS/setup-kerberos.sh

# TigerVNC
source $SCRIPTS/setup-tigervnc.sh

# Anaconda2
source $SCRIPTS/setup-anaconda2.sh

# R
source $SCRIPTS/setup-R.sh

# AFNI and dependencies
source $SCRIPTS/install-libxp.sh
source $SCRIPTS/setup-afni.sh


# NeuroDebian config
# This must be the first step in installing NI applications

source $SCRIPTS/setup-neurodebian.sh
source $SCRIPTS/setup-nd.sh

# FSL
source $SCRIPTS/setup-fsl.sh

# mricron
source $SCRIPTS/setup-mricron.sh

# Nipype
source $SCRIPTS/setup-nipype.sh

exit 0

###########  The following are not done, or may be optional  ###########
# DNS
# source $SCRIPTS/setup-dns.sh

# accounts - passwd & group
# source $SCRIPTS/setup-accounts.sh

# profile
# source $SCRIPTS/setup-profile.sh

# VPN
# source $SCRIPTS/setup-vpn.sh

# filesystems 
# source $SCRIPTS/setup-filesystems.sh

# printing
# source $SCRIPTS/setup-printing.sh

# We don't ever do AFS on Debian/Ubuntu because its PAM does not do PAGs

echo "==== Done."
exit
