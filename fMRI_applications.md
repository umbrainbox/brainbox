## fMRI applications requesteded for initial deployment

```
AFNI
Anaconda 2
Anaconda 3
ANTS
FSL
Freesurfer
mricron
R
RStudio
SPM 8
SPM 12
Matlab
Matlab toolboxes
    Bioinformatics
    Image processing
    Signal processing
    Statistics and machine learning
    Parallel processing
    Wavelet
```