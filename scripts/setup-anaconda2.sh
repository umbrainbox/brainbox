#!/bin/bash

# Configure Anaconda2
echo
echo "=== Installing Anaconda2..."
echo

lsb_rel=$(lsb_release -c | cut -f2)

sudo bash /brainbox/sw/Anaconda2-4.0.0-Linux-x86_64.sh -b -f \
    -p /usr/local/anaconda2-4.0.0
sudo /usr/local/anaconda2-4.0.0/bin/conda update anaconda

echo
echo "=== Done installing Anaconda2."
echo
