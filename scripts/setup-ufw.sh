#!/bin/bash

if [ -z "$BB_TOP" ] ; then
    echo "Need to set BB_TOP"
    exit 1
fi

echo
echo "=== Configuring firewall using ufw..."

sudo ufw enable

###  Enable one ITS machine
# mario
sudo ufw allow from 141.211.228.20     to any port 22 proto tcp
###  Enable the Brainbox ansible server
# bbox2
sudo ufw allow from 192.168.6.52      to any port 22 proto tcp
###  Enable ARC-TS staff
# rincewind - bennet
sudo ufw allow from 141.213.153.133   to any port 22 proto tcp
### enable ARC-TS VPN networks
sudo ufw allow from 141.213.175.88/29 to any port 22 proto tcp
sudo ufw allow from 141.213.171.88/29 to any port 22 proto tcp
###  Enable default user network in lab configurations, not globally
###  as there may be Brainboxes that should not be that exposed
# UM VPN network
# sudo ufw allow from 141.213.168.0/21  to any port 22 proto tcp

echo "=== Done configuring firewall using ufw."
echo

