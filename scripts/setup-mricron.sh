#!/bin/bash

# Configures base

set -e

if [ -z "$BB_TOP" ] ; then
    echo "Need to set BB_TOP"
    exit 1
fi

echo
echo "=== Configuring FSL..."

sudo apt-get install -y mricron mricron-doc

echo "=== Done configuring FSL."
echo
