#!/bin/bash

# Configures repos
echo
echo "=== Configuring Neurodebian repository..."
echo

# Add neurodebian repos
wget -O- http://neuro.debian.net/lists/xenial.us-nh.full \
    | sudo tee /etc/apt/sources.list.d/neurodebian.sources.list
sudo apt-key adv --recv-keys \
    --keyserver hkp://pgp.mit.edu:80 0xA5D32F012649A5A9
sudo apt-get -y update

echo
echo "=== Done configuring repos."
echo
