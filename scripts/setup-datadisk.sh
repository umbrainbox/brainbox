#!/bin/bash

#  Run fdisk:  Options are o, clear memory, n, new partition, p, primary
#              1, first partition, EOL, default begin, EOL, default end,
#              w, write and exit

(echo o; echo n; echo p; echo 1; echo ; echo; echo w) | sudo /dev/fdisk /dev/vdb

#  Create the filesystem
sudo /sbin/mkfs -t ext4 - L data0 -m 0 /dev/vdb1

#  Create an fstab entry
echo "LABEL=data0    /data   ext4  defaults,nosuid   0 0" >> /etc/fstab

#  Create a mount point
mkdir -p /data

#  Mount it
mount /data

#  Need to create user directories here
