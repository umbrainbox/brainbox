#!/bin/bash

# Install and configure libxp, which is an AFNI dependency

echo
echo "=== Installing and configuring libXp"
echo

cd /tmp
mkdir build-libxp
cd build-libxp/

# dpkg build dependencies
sudo apt-get install -y dpkg-dev pkg-config --install-suggests

# AFNI and libXp dependencies
sudo apt-get install -y dh-autoreconf quilt x11proto-print-dev libpng16*
sudo apt-get install -y libx11-dev x11proto-xext-dev libxext-dev libxau-dev xutils-dev
wget https://mirror.umd.edu/ubuntu/pool/main/libx/libxp/libxp_1.0.2-2.diff.gz
wget https://mirror.umd.edu/ubuntu/pool/main/libx/libxp/libxp_1.0.2-2.dsc
wget https://mirror.umd.edu/ubuntu/pool/main/libx/libxp/libxp_1.0.2.orig.tar.gz

# Create the Debian package
dpkg-source -x libxp_1.0.2-2.dsc
cd libxp-1.0.2
dpkg-buildpackage -uc -us -rfakeroot -b
cd ..

# Install the Debian package
sudo dpkg -i libxp-dev_1.0.2-2_amd64.deb libxp6_1.0.2-2_amd64.deb 

cd $BB_TOP
rm -rf /tmp/build-libxp

echo
echo "=== Done installing and configuring libXp ==="
echo
