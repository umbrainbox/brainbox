#!/bin/bash

# Configures base


set -e


if [ -z "$BB_TOP" ] ; then
    echo "Need to set BB_TOP"
    exit 1
fi

echo
echo "=== Configuring ldap utilities..."

# Install packages
apt-get -y install ldap-utils 


echo "=== Done configuring ldap utilities."
echo
