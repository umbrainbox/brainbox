#!/bin/bash

# Verify we have ldapsearch
which ldapsearch > /dev/null
if [ ! $? ]; then
        echo "ERROR: missing ldapsearch program. Please install ldap-utils'"
        exit 1
fi

tmpfile=$(mktemp)

read -p "uniqname to add: " uniqname
echo "Looking up $uniqname UID via ldap.umich.edu..."
ldapsearch -x -LLL -h ldap.umich.edu -b "ou=People,dc=umich,dc=edu" "(uid=$uniqname)" > $tmpfile

UM_UID=`grep uidNumber $tmpfile | awk {'print $2'}`
NAME=`grep displayName $tmpfile | awk {'print $2,$3,$4,$5,$6'} | xargs`

if [ -z "$UID" ] || [ -z "$NAME" ]; then
        echo "ERROR: could not locate $uniqname"
        exit 1
fi

echo "Adding user $uniqname with uid $UM_UID and name '$NAME'"
/usr/sbin/useradd --uid "$UM_UID" -c "$NAME" "$uniqname"

rm $tmpfile

exit
