#!/bin/bash

# Configure AFNI
echo
echo "=== Installing AFNI..."
echo

#  libgsl needed by AFNI
sudo apt-get install -y libgsl2

# wget -O afni_linux_openmp_64.tgz https://afni.nimh.nih.gov/pub/dist/tgz/linux_openmp_64.tgz
cd /usr/local
tar xzf /brainbox/sw/afni-16.3.18.tar.gz
AFNI_VER=$(linux_openmp_64/afni -ver | cut -d' ' -f 8 | sed 's/AFNI_\(.*\))/\1/')
mv linux_openmp_64 /usr/local/afni-$AFNI_VER
cd -

echo
echo "=== Done installing AFNI."
echo
