#/bin/bash

# Installs and configures ssh service
# Make sure that the ufw firewall is installed and configured first

echo
echo "=== Configuring ssh service"

# Set the ClientAlive parameters so people's ssh sessions don't go dead.
cd /etc/ssh
sed -i.orig 's/^TCPKeepAlive yes$/TCPKeepAlive yes\nClientAliveInterval 60\nClientAliveCountMax 30/' sshd_config

sudo systemctl enable ssh.service
sudo systemctl start ssh

echo "=== Done configuring ssh service"
echo

