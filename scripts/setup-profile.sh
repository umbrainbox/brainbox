#!/bin/bash

# Configures profile


set -e


if [ -z "$BB_TOP" ] ; then
    echo "Need to set BB_TOP"
    exit 1
fi

SKEL=/nfs/brainbox/skel

echo
echo "=== Configuring profile..."

if [ -d $SKEL ] ; then
    cp $SKEL/brainbox.sh /etc/profile.d
fi

echo "=== Done configuring profile."
echo
