#!/bin/bash

echo
echo "=== Installing TigerVNC..."
echo

cd /
tar xzvf /brainbox/sw/tigervnc-Linux-x86_64-1.6.0.tar.gz

echo
echo "=== Done installing TigerVNC."
echo
