#!/bin/bash

# Configures base

set -e

if [ -z "$BB_TOP" ] ; then
    echo "Need to set BB_TOP"
    exit 1
fi

echo
echo "=== Configuring FSL..."

sudo apt-get install -y fsl-5.0-core fsl-feeds

echo "=== Done configuring FSL."
echo
