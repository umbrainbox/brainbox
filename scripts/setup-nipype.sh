#!/bin/bash

# Configures base

set -e

if [ -z "$BB_TOP" ] ; then
    echo "Need to set BB_TOP"
    exit 1
fi

echo
echo "=== Configuring Nipype..."

sudo apt-get install -y python-nipype python-nipype-doc python-dateutil

echo "=== Done configuring Nipype."
echo
