#!/bin/bash

# Configures accounts


set -e


if [ -z "$BB_TOP" ] ; then
    echo "Need to set BB_TOP"
    exit 1
fi

echo
echo "=== Configuring accounts..."

# Have copy of /etc/passwd from login.itd.umich.edu under root/etc/

# Copy into place
#cp root/etc/passwd /etc/UM-passwd

## Adding the user
# Check if user is in /etc/UM-passwd: 
# grep uniqname /etc/UM-passwd 
# If exists: 
#   get UID and name fields
#   adduser uniqname
# If not: 
#   sync a new version of /etc/UM-passwd
#   grep uniqname /etc/UM-passwd
#       If not:
#           error: bad uniqname
#       Else:
#           adduser uniqname


echo "=== Done configuring accounts."
echo
