#!/bin/bash

# Configures filesystems


set -e


if [ -z "$BB_TOP" ] ; then
    echo "Need to set BB_TOP"
    exit 1
fi

echo
echo "=== Configuring filesystems..."

#  NOTE:  This must be run after setup-utils.sh, which installs the
#         NFS client software

# Create mount points
sudo mkdir -p /nfs/brainbox

# Add mounts
# installation source mount
sudo mount -t nfs -onosuid,tcp,nfsvers=3 \
    fluxsoftware.value.storage.umich.edu:/fluxsoftware/brainbox \
    /nfs/brainbox

echo "=== Done configuring filesystems."
echo
