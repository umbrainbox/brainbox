#!/bin/bash

# Configures kerberos
echo
echo "=== Configuring kerberos..."
echo

cp root/etc/krb5.conf /etc/krb5.conf
apt-get -y install krb5-user libpam-krb5

echo
echo "=== Done configuring kerberos."
echo

