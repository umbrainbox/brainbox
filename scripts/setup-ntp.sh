#!/bin/bash

# Configures ntp 
echo
echo "=== Configuring ntp..."
echo

# Install packages
apt-get -y install ntp ntpdate

# Copy config
cp $BB_TOP/root/etc/ntp.conf /etc/ntp.conf

# Restart service
systemctl restart ntp
sleep 1

# Test
ntpq -p

echo
echo "=== Done configuring ntp."
echo
