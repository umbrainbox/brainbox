#!/bin/bash

# Configures firewall


set -e


if [ -z "$BB_TOP" ] ; then
    echo "Need to set BB_TOP"
    exit 1
fi

echo
echo "=== Configuring firewall..."

# Install packages
apt-get -y install iptables-persistent

# Add netfilter-persistent Startup
invoke-rc.d netfilter-persistent save

# Stop netfilter-persistent Service
service netfilter-persistent stop

echo "=== Done configuring firewall."
echo
