#!/bin/bash

set -e


if [ -z "$BB_TOP" ] ; then
    echo "Need to set BB_TOP"
    exit 1
fi

echo "=== Configuring DNS..."

# Note: Change to use NetworkManager instead
# Add to /etc/resolvconf/resolv.conf.d/head so 
# the lines get into /etc/resolv.conf when it is generated
echo "nameservers 141.211.125.17" >> /etc/resolvconf/resolv.conf.d/head
echo "nameservers 141.211.144.17" >> /etc/resolvconf/resolv.conf.d/head

# Reset network interface
ifdown ens3 && ifup ens3

# Check
cat /etc/resolv.conf
nslookup www.umich.edu

echo "=== Done with DNS."
