#!/bin/bash

# Update to current
echo
echo "==== Begin update and install apt packages..."
echo
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
sudo apt autoremove

# Install packages
#  admin editor is vim
#  user editors are emacs and gedit
#  ncurses-term and aptitude for ad hoc package exploration
#  dstat for performance stats and troubleshooting
#  rpcbind, nfs-common for ValueStorage and Turbo
#  git-gui, gitk, just 'cause
#  cmake because it is the ND build system
#  ldap-utils for user creation
apt-get -y install vim emacs gedit m17n-docs ncurses-term aptitude \
                   dstat rpcbind nfs-common git-gui gitk cmake ldap-utils

echo
echo "==== End update and install apt packages..."
echo

