#!/bin/bash

# Configure R


# set -e


if [ -z "$BB_TOP" ] ; then
    echo "Need to set BB_TOP"
    exit 1
fi

echo
echo "=== Installing R..."
echo

lsb_rel=$(lsb_release -c | cut -f2)
echo "deb https://cran.mtu.edu/bin/linux/ubuntu $lsb_rel/" >> /etc/apt/sources.list
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9
sudo apt-get update -y

sudo apt-get remove -y libpng16-dev libpng16-devtools libpng16-tools

sudo apt-get install -y r-base r-base-dev \
    texlive-base texlive-latex-base texlive-generic-recommended \
    texlive-fonts-recommended texlive-fonts-extra texlive-extra-utils \
    texlive-latex-recommended texlive-latex-extra fonts-inconsolata

# RStudio dependencies
sudo apt-get install libjpeg62 libgstreamer0.10-0 libgstreamer-plugins-base0.10-0
sudo dpkg -i /brainbox/sw/rstudio-0.99.902-amd64.deb

echo
echo "=== Done installing R."
echo
