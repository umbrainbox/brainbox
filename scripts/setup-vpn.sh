#!/bin/bash

# Configures VPN


set -e


if [ -z "$BB_TOP" ] ; then
    echo "Need to set BB_TOP"
    exit 1
fi

echo
echo "=== Configuring VPN..."

# Download VPN
# wget https://www.itcom.itd.umich.edu/vpn/software/vpnclient-linux-x86_64-4.8.02.0030-k9.tar.gz
# note: wget seems to fail

# Extract
tar xf vpnclient-linux-x86_64-4.8.02.0030-k9.tar.gz
cd vpnclient

# Install
./vpn_install 2>&1 | tee log.install

# Requires reboot

echo "=== Done configuring VPN."
echo
