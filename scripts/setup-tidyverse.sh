#!/bin/bash

# Setup for the R package tidyverse; requested by berent group
# This may be something we should leave to each group, but these
#   are the system programs/libraries that seem needed to make
#   it installable.

apt-get install curl openssl libcurl4-openssl-dev libssl-dev

# Note:  No error checking or testing.  Fix that if this goes
#        live for everyone.
cd /tmp
echo "install.packages('tidyverse')" setup-tidyverse.R
R CMD BATCH setup-tidyverse.R
